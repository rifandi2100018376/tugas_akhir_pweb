<?php
    $rangking=array("sasa"=>3,"sisi"=>2,"susu"=>4,"soso"=>1);
    echo "<B>Rangking sebelum diurutkan</B>";
    echo "<PRE>";
    foreach ($rangking as $nama =>$nomor)
    {
        echo "$nama mendapat peringkat $nomor";
        echo "<br />";
    }
    echo "</PRE>";
    asort($rangking);
    echo "<B>Rangking setelah diurutkan dengan asort()</B>";
    echo "<PRE>";
    foreach ($rangking as $nama =>$nomor)
    {
        echo "$nama mendapat peringkat $nomor";
        echo "<br />";
    }
    echo "</PRE>";
    arsort($rangking);
    reset($rangking);
    echo "<B>Rangking setelah diurutkan dengan arsort()</B>";
    echo "<PRE>";
    foreach ($rangking as $nama =>$nomor)
    {
        echo "$nama mendapat peringkat $nomor";
        echo "<br />";
    }
    echo "</PRE>";
?>
<center><br>Kembali ke <a href="index.html">Beranda</a></center>